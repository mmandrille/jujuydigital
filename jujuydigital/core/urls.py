from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'ws'
urlpatterns = [
    url(r'^$', views.home, name='home'),
]