#Import Standards
from django.db import models

#Imports de plugins
from tinymce.models import HTMLField

#Import Personales
from .choices import TIPO_DOCUMENTOS, NACIONALIDADES, TIPO_PROVEEDOR, TIPO_INSCRIPCION

# Create your models here.
class Persona(models.Model):
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=0)
    num_doc = models.IntegerField('Num de Documento')
    nacionalidad = models.IntegerField(choices=NACIONALIDADES, default=200)
    apellidos = models.CharField('Apellidos', max_length=50, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=50, blank=True, null=True)
    domicilio = models.CharField('Domicilio', max_length=25, blank=True, null=True)
    telefono = models.CharField('Numero de Contacto', max_length=25, blank=True, null=True)
    email = models.EmailField('Correo Electronico', null=True, blank=True)
    casado = models.BooleanField(verbose_name="Estado Civil (Casado?)", default=False)
    #solo en apoderados, representantes locales y socios
    cuit = models.IntegerField('Cuit')
    limite_oferta = models.IntegerField('Limite Oferta Electronica')
    autorizado_oferta = models.IntegerField('Autorizado para Oferta Electronica')
    opciones = models.IntegerField('Opciones')
    def __str__(self):
        return self.apellidos + ' ' + self.nombres

class Domicilio(models.Model):
    pais = models.IntegerField(choices=NACIONALIDADES, default=200)
    provincia = models.CharField('Provincia', max_length=50, blank=True, null=True)
    departamento = models.CharField('Departamento', max_length=50, blank=True, null=True)
    localidad = models.CharField('Localidad', max_length=50, blank=True, null=True)
    codigo_postal = models.CharField('Codigo Postal', max_length=50, blank=True, null=True)
    calle = models.CharField('Calle', max_length=50, blank=True, null=True)
    numero = models.CharField('Numero', max_length=50, blank=True, null=True)
    extra = models.CharField('Informacion Extra', max_length=50, blank=True, null=True)
    telefono = models.CharField('Telefono', max_length=50, blank=True, null=True)

class Constitucion(models.Model):
    lugar = models.CharField('Lugar de Constitucion', max_length=100, blank=True, null=True)
    fecha = models.DateTimeField('Fecha de Constitucion', blank=True, null=True)

class Inscripcion(models.Model):
    tipo_inscripcion = models.IntegerField(choices=TIPO_INSCRIPCION, default=0)
    numero = models.IntegerField('Numero de Inscripcion')

class Rubros(models.Model):
    nombre = models.CharField('Nombre', max_length=100, blank=True, null=True)
    detalle = HTMLField()
    def __str__(self):
        return self.nombre

class Proveedor(models.Model):
    tipo_proveedor = models.IntegerField(choices=TIPO_PROVEEDOR, default=0)
    razon_social = models.CharField('Razon Social', max_length=100, blank=True, null=True)
    cuit = models.IntegerField('Cuit')#Bloquear a solo 11 digitos
    email = models.EmailField('Correo Electronico Institucional', null=True, blank=True)
    domicilio_legal = models.ForeignKey(Domicilio, on_delete=models.CASCADE, related_name="proveedores")
    rubros = models.ManyToManyField(Rubros, blank=True)
    constitucion = models.ForeignKey(Constitucion, on_delete=models.CASCADE, related_name="proveedores", null=True, blank=True)
    inscripcion = models.ForeignKey(Inscripcion, on_delete=models.CASCADE, related_name="proveedores", null=True, blank=True)
    apoderados = models.ManyToManyField(Persona, blank=True)
    representates = models.ManyToManyField(Persona, blank=True)
    socios = models.ManyToManyField(Persona, blank=True)
    def __str__(self):
        return self.razon_social